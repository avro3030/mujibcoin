## At difficulty level 4, output is following:

![Scheme](https://bytebucket.org/avro3030/mujibcoin/raw/6ef796ab4cd30995c74a866a200818ec8273b7f9/output.png)


## ➜  mujibCoin git:(master) ✗ node mujibCoin.js
{
    "index": 0,
    "timestamp": "01/01/2018",
    "data": {
        "amount": 20
    },
    "previousHash": "genesis hash",
    "hash": "39e017fe885e66a281dcebad1992f49c1ea913156887b190cdabfd72e0526776",
    "mine": 0
}
Mining started......
## Attempts: 14443
Mining done. hash: 0000f9909b05736651777f44e2eb2ab8a6ffaa4ba5861b2d8f5f4608bcf7c42b
## Attempts: 120786
Mining done. hash: 0000937364cfc5350505abcb46459952b4f70cde71c5b6a713d1efc58c061ccd
## Attempts: 135012
Mining done. hash: 0000ef671d939be21be7f9b595a45bc873ebb1c3762232c12ab582ddcb2a909e
## Attempts: 16210
Mining done. hash: 00000a327bca441b95e7dd1bff26d4ebc36b6ef3205f4ee00847c896854a37b2
Is block valid true
## After tempering with the Block
Is block valid false
{
    "chain": [
        {
            "index": 0,
            "timestamp": "01/01/2018",
            "data": {
                "amount": 20
            },
            "previousHash": "genesis hash",
            "hash": "39e017fe885e66a281dcebad1992f49c1ea913156887b190cdabfd72e0526776",
            "mine": 0
        },
        {
            "index": 1,
            "timestamp": "02/01/2018",
            "data": {
                "amount": 40
            },
            "previousHash": "39e017fe885e66a281dcebad1992f49c1ea913156887b190cdabfd72e0526776",
            "hash": "0000f9909b05736651777f44e2eb2ab8a6ffaa4ba5861b2d8f5f4608bcf7c42b",
            "mine": 14443
        },
        {
            "index": 2,
            "timestamp": "02/01/2018",
            "data": {
                "amount": 2000
            },
            "previousHash": "0000f9909b05736651777f44e2eb2ab8a6ffaa4ba5861b2d8f5f4608bcf7c42b",
            "hash": "6dca00bff467eda8a242f0fe5e2c088ccc517727f04a92609ea8908ac860193f",
            "mine": 120786
        },
        {
            "index": 3,
            "timestamp": "02/01/2018",
            "data": {
                "amount": 6
            },
            "previousHash": "0000937364cfc5350505abcb46459952b4f70cde71c5b6a713d1efc58c061ccd",
            "hash": "0000ef671d939be21be7f9b595a45bc873ebb1c3762232c12ab582ddcb2a909e",
            "mine": 135012
        },
        {
            "index": 4,
            "timestamp": "02/01/2018",
            "data": {
                "amount": 8
            },
            "previousHash": "0000ef671d939be21be7f9b595a45bc873ebb1c3762232c12ab582ddcb2a909e",
            "hash": "00000a327bca441b95e7dd1bff26d4ebc36b6ef3205f4ee00847c896854a37b2",
            "mine": 16210
        }
    ],
    "difficulty": 4
}


## mujib: 3389.505ms