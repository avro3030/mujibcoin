const SHA256 = require('crypto-js/sha256');

class Block {
    constructor(index, timestamp, data, previousHash){
        this.index = index;
        this.timestamp =timestamp;
        this.data = data;
        this.previousHash = previousHash;
        this.hash = this.calculateHash();
        this.mine = 0;
    }

    calculateHash(){
        return SHA256(this.index + this.timestamp + this.previousHash + JSON.stringify(this.data) + this.mine).toString()
    }
    mineBlock(difficulty){
        while(this.hash.substring(0,difficulty) !== '0'.repeat(difficulty)){
            this.mine++;
            this.hash = this.calculateHash();
        }

        console.log(`Attempts: ${this.mine}`)

    }
}

class BlockChain {
    constructor(){
        this.chain = [this.genesisBlock()];
        this.difficulty = 4;
    }

    genesisBlock(){
        return new Block(0,'01/01/2018',{amount: 20},'genesis hash');
    }
    getLatestBlock(){
        return this.chain[this.chain.length -1];
    }

    addNewBlock(newBlock){
        // if(!hasValidHashRef(newBlock.previousHash)){
        //     return false;
        // }
        // if(!solveProblem(newBlock)){
        //     return false;
        // }
        // this.chain.push(newBlock);
        // return true;
        newBlock.previousHash = this.getLatestBlock().hash;
        // newBlock.hash = newBlock.calculateHash();
        newBlock.mineBlock(this.difficulty);
        this.chain.push(newBlock)
    }

    isBlockchainStillvalid(){
        for(let i=1; i < this.chain.length ; i++){
            const currentBlock = this.chain[i];
            const previousBlock = this.chain[i-1];
            if(currentBlock.hash !== currentBlock.calculateHash()) return false;
            if(currentBlock.previousHash !== previousBlock.hash ) return false;
        }
        return true;
    }

    // hasValidHashRef(prevHash){
    //     return this.getLatestBlock().hash() === prevHash;
    // }
    //
    // solveProblem(block){
    //     return true;
    // }

}

console.time('mujib');

const mujibCoin = new BlockChain();

console.log(JSON.stringify(mujibCoin.genesisBlock(),null,4));

console.log('Mining started......');

mujibCoin.addNewBlock(new Block(1,'02/01/2018',{amount: 40}));
console.log(`Mining done. hash: ${mujibCoin.getLatestBlock().hash}`);
mujibCoin.addNewBlock(new Block(2,'02/01/2018',{amount: 20}));
console.log(`Mining done. hash: ${mujibCoin.getLatestBlock().hash}`);
mujibCoin.addNewBlock(new Block(3,'02/01/2018',{amount: 6}));
console.log(`Mining done. hash: ${mujibCoin.getLatestBlock().hash}`);
mujibCoin.addNewBlock(new Block(4,'02/01/2018',{amount: 8}));
console.log(`Mining done. hash: ${mujibCoin.getLatestBlock().hash}`);

console.log(`Is block valid ${mujibCoin.isBlockchainStillvalid()}`);

mujibCoin.chain[2].data = { amount: 2000};
mujibCoin.chain[2].hash = mujibCoin.chain[2].calculateHash();

console.log(`Is block valid ${mujibCoin.isBlockchainStillvalid()}`);

console.log(JSON.stringify(mujibCoin,null,4));

console.timeEnd('mujib');

